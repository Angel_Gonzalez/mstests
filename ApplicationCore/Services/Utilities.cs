﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApplicationCore.Services
{
    public class Utilities
    {

        public int ContarDivisores(int numero)
        {
            var contador = 0;
            for (int i = 1; i < numero; i++)
            {
                if (numero % i == 0)
                    contador++;
            }
            return contador;

        }

        public class Product
        {
            public int Code { get; set; }
            public string Desc { get; set; }
            public double Value { get; set; }
        };

        public double ObtenerValorProducto(int codigo)
        {
            var listProducts = new List<Product>()
            {
                new Product {Code = 1250, Desc="MOUSE 3 BOTONES", Value = 15500},
                new Product {Code = 1260, Desc="IMPRESORA LASER", Value = 678000},
                new Product {Code = 1270, Desc="MEMORIA USB 20 GB", Value = 35000},
                new Product {Code = 1280, Desc="DISCO DURO 500 GB", Value = 180000},
                new Product {Code = 1290, Desc="MONITOR 14 PULGADAS", Value = 280000},

            };

            var product = listProducts.Where(x => x.Code == codigo).FirstOrDefault();
            if (product == null)
                return 0;

            return product.Value;
        }



        public double CalcularSubtotal(double valor, int cantidad)
        {
            var Resultado_Subtotal = valor * cantidad;
            return Resultado_Subtotal;
        }


        public double CalcularDescuento(double subtotal)
        {
            double Resultado_Des= 0;
           
            if (subtotal >= 500000 && subtotal <= 1000000)
            {
                Resultado_Des = (subtotal * 5) / 100;
                return Resultado_Des;
            }
            else if (subtotal > 1000000)
            {
                Resultado_Des = (subtotal * 10) / 100;
                return Resultado_Des;
            }

            else { 
            return Resultado_Des;
            }
        }
        public double CalcularIVA(double subtotal)
        {
            var Resultado_Iva = (subtotal  * 19) /100;
            return Resultado_Iva;

        }
        public double CalcularTotal(double subtotal, double descuento, double impuesto)
        {

            double Resultado_Total;
            double Iva;
            double Descuento;

            if (subtotal >= 500000 && subtotal <= 1000000)
            {
                Iva = (subtotal * 19) /100;
                Descuento = (subtotal * 5) / 100;
                Resultado_Total = (subtotal - Descuento) + Iva;

                return Resultado_Total;

            }
            else if (subtotal > 1000000)
            {
                Iva = (subtotal * 19) /100;
                Descuento = (subtotal * 10) / 100;
                Resultado_Total = (subtotal - Descuento) + Iva;
                return Resultado_Total;
            }
            else
            {
                return Resultado_Total = subtotal;
            }



        }
    }
}

