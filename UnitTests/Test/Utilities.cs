﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace UnitTests.Test
{
    [TestClass]
    public class Utilities
    {
        [TestMethod]
        public void ContarDivisoresDeSeis()
        {
            //Arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var numero = 6;

            //Act
            var result = utilities.ContarDivisores(numero);

            //Assert
            Assert.AreEqual(3, result);

        }

        [TestMethod]
        public void ObtenerValorMouse()
        {
            //Arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var product = 1290;

            //Act
            var result = utilities.ObtenerValorProducto(product);

            //Assert
            Assert.AreEqual(280000, result);
        }

        [TestMethod]
        public void SubTotal()
        {
            //arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var cantidad = 2;
            var valor = 280000;

            //Act
            var Resultado_Subtotal = utilities.CalcularSubtotal(cantidad,valor);
           

            //Assert
            Assert.AreEqual(560000, Resultado_Subtotal);
        }

        [TestMethod]
        public void Descuento()
        {

            //arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var subtotal = 560000;

            //Act
            var Resultado_Des = utilities.CalcularDescuento(subtotal);


            //Assert
            Assert.AreEqual(28000,Resultado_Des);

        }

        [TestMethod]
        public void IVA()
        {
            //arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var subtotal = 560000;

            //Act
            var Resultado_Iva = utilities.CalcularIVA(subtotal);


            //Assert
            Assert.AreEqual(106400, Resultado_Iva);
        }

        [TestMethod]
        public void TOTAL()
        {
            //arrange
            ApplicationCore.Services.Utilities utilities = new ApplicationCore.Services.Utilities();
            var subtotal = 560000;
            var descuento = 28000;
            var iva = 106400;

            //Act
            var Resultado_Total = utilities.CalcularTotal(subtotal,descuento,iva);


            //Assert
            Assert.AreEqual(638400, Resultado_Total);
        }

    }
}
